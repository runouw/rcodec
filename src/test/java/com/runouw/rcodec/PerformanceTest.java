/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.rcodec;

import com.runouw.rcodec.CoderNode;
import com.runouw.rcodec.BeautifyRules;
import com.runouw.hypercodec.BJSONDecoder;
import com.runouw.hypercodec.BJSONEncoder;
import com.runouw.hypercodec.JSONDecoder;
import com.runouw.hypercodec.JSONEncoder;
import com.runouw.rcodec.obj.Item;
import com.runouw.rcodec.obj.ItemList;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.junit.Test;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;
import org.openjdk.jmh.runner.options.TimeValue;

/**
 *
 * @author Robert
 */
public class PerformanceTest {

    private static final ItemList ITEMS;
    private static final byte[] AS_BYTES;
    private static final String JSON_MINIFIED;
    private static final String JSON_EXPANDED;
    private static final Path AS_JSON_FILE;
    private static final Path AS_BJSON_FILE;
    private static final Path COMPLEX_JSON_FILE = Paths.get("src/test/resources/sample.json");
    private static final Map<String, Object> DECODED_OBJ;

    static {
        try {
            ITEMS = new ItemList();
            for (int h = 0; h < 1000; h++) {
                ITEMS.items.add(Item.generate());
            }

            AS_BYTES = ITEMS.encode(new CoderNode()).toBytes();
            JSON_MINIFIED = ITEMS.encode(new CoderNode()).withBeautify(BeautifyRules.MINIFIED).toString();
            JSON_EXPANDED = ITEMS.encode(new CoderNode()).withBeautify(BeautifyRules.SPACES_X_4).toString();

            AS_JSON_FILE = Paths.get("benchmark.json");
            AS_BJSON_FILE = Paths.get("benchmark.bjson");

            ITEMS.encode(new CoderNode()).writeJSONFile(AS_JSON_FILE);
            ITEMS.encode(new CoderNode()).writeBJSONFile(AS_BJSON_FILE);

            try (FileChannel fc = FileChannel.open(AS_JSON_FILE, StandardOpenOption.READ)) {
                final Charset cs = Charset.forName("UTF8");
                final ByteBuffer data = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());

                DECODED_OBJ = JSONDecoder.decode(cs.decode(data));
            }
        } catch (Exception t) {
            throw new RuntimeException(t);
        }
    }    

    //@Test
    public void launchBenchmark() throws Exception {
        Options opt = new OptionsBuilder()
                // Specify which benchmarks to run. 
                // You can be more specific if you'd like to run only one benchmark per test.
                .include(this.getClass().getName() + ".*")
                // Set the following options as needed
                .mode(Mode.Throughput)
                .timeUnit(TimeUnit.SECONDS)
                .warmupTime(TimeValue.milliseconds(200))
                .warmupIterations(20)
                .measurementTime(TimeValue.milliseconds(1000))
                .measurementIterations(10)
                .threads(1)
                .forks(1)
                .shouldFailOnError(true)
                .shouldDoGC(true)
                .jvmArgs("-XX:+UnlockDiagnosticVMOptions", "-XX:+PrintInlining")
                //.addProfiler(WinPerfAsmProfiler.class)
                .build();

        new Runner(opt).run();
    }

    /*
    @Benchmark
    public void timeEncodeNormal(){
        Item.USE_GENERIC_ARRAY = false;
        ITEMS.encode(new CoderNode()).toBytes();
    }
    @Benchmark
    public void timeEncodeWithArrays(){
        Item.USE_GENERIC_ARRAY = true;
        ITEMS.encode(new CoderNode()).toBytes();
    }
     */
    //@Benchmark
    public void timeEncodeBJSON() {
        ITEMS.encode(new CoderNode()).toBytes();
    }

    //@Benchmark
    public void timeEncodeJSONMinified() {
        ITEMS.encode(new CoderNode()).withBeautify(BeautifyRules.MINIFIED).toString();
    }

    //@Benchmark
    public void timeEncodeJSONExpanded() {
        ITEMS.encode(new CoderNode()).withBeautify(BeautifyRules.SPACES_X_4).toString();
    }

    //@Benchmark
    public void timeDecodeBJSON() {
        ItemList items = new ItemList();

        items.decode(new CoderNode().fromBytes(AS_BYTES));
    }

    //@Benchmark
    public void timeXFromBJSON() {
        new CoderNode().fromBytes(AS_BYTES);
    }

    //@Benchmark
    public void timeDecodeJSONMinified() {
        ItemList items = new ItemList();

        items.decode(new CoderNode().fromString(JSON_MINIFIED));
    }

    //@Benchmark
    public void timeDecodeJSONExpanded() {
        ItemList items = new ItemList();

        items.decode(new CoderNode().fromString(JSON_EXPANDED));
    }

    //@Benchmark
    public void timeXFromJSONMinified() {
        new CoderNode().fromString(JSON_MINIFIED);
    }

    //@Benchmark
    public void timeXFromJSONExpanded() {
        new CoderNode().fromString(JSON_EXPANDED);
    }

    //@Benchmark
    public void timeDecodeJSONFile() throws IOException {
        ItemList items = new ItemList();

        items.decode(new CoderNode().fromPath(AS_JSON_FILE));
    }
    
    //@Benchmark
    public void timeDecodeHypercodecJSONFile() throws IOException {
        ItemList items = new ItemList();
        
        items.decode(CoderNode.decode(AS_JSON_FILE, Charset.forName("UTF8")));
    }

    //@Benchmark
    public void timeDecodeBJSONFile() throws IOException {
        ItemList items = new ItemList();

        items.decode(new CoderNode().fromPath(AS_BJSON_FILE));
    }

    //@Benchmark
    public void timeEncodeHypercodecJSON() throws IOException {
        final CharBuffer writeBuff = ByteBuffer.allocateDirect(1024 * 1024)
                .asCharBuffer();

        JSONEncoder.encode(writeBuff, DECODED_OBJ);

        writeBuff.flip();
        writeBuff.toString();
    }
    
    //@Benchmark
    public void timeEncodeHypercodecBJSON() throws IOException {
        final ByteBuffer writeBuff = ByteBuffer.allocateDirect(1024 * 1024);
        
        BJSONEncoder.encodeHeader(writeBuff);
        BJSONEncoder.encode(writeBuff, DECODED_OBJ);
        writeBuff.flip();
    }

    //@Benchmark
    public void timeDecodeHypercodecBJSON() throws IOException {
        try (final FileChannel fc = FileChannel.open(AS_BJSON_FILE, StandardOpenOption.READ)) {
            final ByteBuffer data = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());

            BJSONDecoder.checkHeader(data);
            BJSONDecoder.decode(data);
        }
    }

    @Benchmark
    public void timeDecodeHypercodecJSON() throws IOException {
        try (final FileChannel fc = FileChannel.open(AS_JSON_FILE, StandardOpenOption.READ)) {
            final ByteBuffer data = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());

            JSONDecoder.decode(Charset.forName("UTF8").decode(data));
        }
    }
}
