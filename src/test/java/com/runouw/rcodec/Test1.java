/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.rcodec;

import com.runouw.rcodec.Indentation;
import com.runouw.rcodec.CoderNode;
import com.runouw.rcodec.BeautifyRules;
import java.util.Base64;
import org.junit.Test;

/**
 *
 * @author Robert
 */
public class Test1 {
    private static final String IMAGE_BASE_64 = "iVBORw0KGgoAAAANSUhEUgAAADUAAAAuCAYAAACI91EoAAACMUlEQVR42u2ZIXPCQBCF8RXISkRFRQUyAlGBqKhAIisqKiqQFYhKRH8IEoms5AdU8BOQlUjKbvMyj+WOxGWXyc28uZAcM/vl7e1dkl6va127rlY8DA+sq4B5Gz2qFq8T7cOCSeDr9xeVwAAICuca3GEoCwPQEGAccMqh1LkwUKlUY+BQKWiLAwPtlrO4VRBQDLPZbqvjw24VI+1yUFIoADWdF3GgBnfFgcVpCCgrt1CA2O/3lTY/Wz3HUBbMJRDDzL6WKnZKwIrR5F8GTHo5B0fdAMEVC2PBROwiuwlH3UBJQACCU0i3j9WTKgWIsXwz3AAhIA4O5VqApMqx4C7/F9daXa8YiNMHCyqKgDo2HmhvoRhGgcpxrS7GqblTBwQouFqBlL3dZbQOyUAITs8fA2agqswLCLmDisg7D66QrT0vJe90GTyX/ROXjurf9M/2haLFeKhq1SncVd55A/RkMTbzR6DsJhgutQqV25UDiuedffxgqNS7DFcvVnh+8YMiXMPvFJTLnXkOSIRlgNMPCvMqDO8fUkUEMDJ/ZLF2DWYr2v3zZ1aAErmH4sBv59+qHJT7FAQQQKzqXHMJeAnqEpCVjHcDdgmKXWgC5Q4sBWTH1AHKgh0Kqg7QFVAOqgmYW5cYarr+VaFI5AJ17xIHylAibGy5ZHPq8XW365VNQ3GBPwxgDB+H2OACDGnFX0IAFA6Ki4D0KahQMKl05O9V4aEYLmzKNXHtaoAsXK9rXTtrf1kQk+nEE+iRAAAAAElFTkSuQmCC";
    
    @Test
    public void test1(){
        CoderNode node = new CoderNode()
                .set("red", 0xFF)
                .set("green", 0xFF)
                .set("blue", 0xFF)
                .set("alpha", 0xFF)
                .set("bytes", Base64.getDecoder().decode(IMAGE_BASE_64))
                .withArray("arr", arr -> {
                    for(int i=0;i<100;i++){
                        arr.add(i*i);
                    }
                });
        
        System.out.println(node.toString());
        System.out.println(node.withBeautify(new BeautifyRules(Indentation.SPACES, 2)).toString());
    }
}
