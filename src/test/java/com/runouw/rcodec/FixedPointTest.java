/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.rcodec;

import java.nio.CharBuffer;
import junit.framework.Assert;
import org.junit.Test;

/**
 *
 * @author zmichaels
 */
public class FixedPointTest {
    @Test
    public void testFixedPoint() {
        Fixed f0 = new Fixed(3.14);
        Fixed f1 = Fixed.parseFixed("-5902.325103623392");
        Fixed f2 = Fixed.parseFixed("42");
        Fixed f3 = Fixed.decodeFixed(CharBuffer.wrap("1337\0"));
        Fixed f4 = Fixed.decodeFixed(CharBuffer.wrap("-99.9"));
        Fixed f5 = Fixed.parseFixed("999/1000");
        Fixed f6 = Fixed.decodeFixed(CharBuffer.wrap("1/10"));
                                
        Assert.assertTrue(Math.abs(f0.doubleValue() - 3.14) < 0.01);
        Assert.assertEquals("-5902.325103623392", Double.toString(f1.doubleValue()));
        Assert.assertEquals("42", f2.toString());        
        Assert.assertEquals("1337", f3.toString());
        
        Assert.assertEquals("-999/10", f4.toString());
        Assert.assertEquals("999/1000", f5.toString());
        Assert.assertEquals("0.999", Double.toString(f5.doubleValue()));
        
        Assert.assertEquals("1/10", f6.toString());
        Assert.assertEquals("0.1", Double.toString(f6.doubleValue()));        
    }
}
