/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.rcodec;

import com.runouw.rcodec.CoderNode;
import org.junit.Test;

/**
 *
 * @author runou
 */
public class Test4 {

    private static class MyObject{
        private int myInt = 0;
        private double myDouble = 0;
        private Rectangle rect = new Rectangle(0, 0, 100, 100);

        public CoderNode encode(){
            return new CoderNode()
                    .set("myInt", myInt)
                    .set("myDouble", myDouble)
                    .set("rect", rect.encode())
                    ;
        }

        public MyObject decode(CoderNode node){
            node
                .ifInt("myInt", this::setMyInt)
                .ifDouble("myDouble", this::setMyDouble)
                .withNode("rect", r -> setRect(Rectangle.from(r)));

            return this;
        }

        public void setMyDouble(double myDouble) {
            this.myDouble = myDouble;
        }

        public void setMyInt(int myInt) {
            this.myInt = myInt;
        }

        public void setRect(Rectangle rect) {
            this.rect = rect;
        }

        public MyObject from(CoderNode node){
            return new MyObject().decode(node);
        }
    }
    private static class Rectangle{
        private double x;
        private double y;
        private double width;
        private double height;

        public Rectangle(double x, double y, double width, double height) {
            this.x = x;
            this.y = y;
            this.width = width;
            this.height = height;
        }

        public CoderNode encode(){
            return encode(new CoderNode());
        }
        public CoderNode encode(CoderNode node){
            return node
                    .set("id", "Rectangle")
                    .set("x", x)
                    .set("y", y)
                    .set("width", width)
                    .set("height", height)
                    ;
        }

        public static Rectangle from(CoderNode node){
            return new Rectangle(
                    node.getDouble("x", 0.0),
                    node.getDouble("y", 0.0),
                    node.getDouble("width", 0.0),
                    node.getDouble("height", 0.0)
            );
        }
    }

    @Test
    public void test4(){

        MyObject obj = new MyObject();
        obj.setMyDouble(50);
        obj.setMyInt(20);
        obj.rect.x = 10;
        obj.rect.y = 10;
        obj.rect.width = 33;
        obj.rect.height = 44;
        /*
        CoderNode root = new CoderNode();


        root.set("bytes", new byte[]{
            (byte) 0xF,
            (byte) 0xFF,
            (byte) 0xFFF,
            (byte) 0xFFFF,
        });

        root.set("obj", obj.encode());
        */
        System.out.println(obj.encode());
        System.out.println(new MyObject().decode(obj.encode()).encode());
    }


}
