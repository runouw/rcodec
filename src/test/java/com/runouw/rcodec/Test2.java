/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.rcodec;

import com.runouw.rcodec.CoderNode;
import org.junit.Test;

/**
 *
 * @author Robert
 */
public class Test2 {
    @Test
    public void test2(){
        CoderNode node = new CoderNode()
            .set("type", "argb")
            .set("red", 0xFF)
            .set("green", 0xFF)
            .set("blue", 0xFF)
            .set("alpha", 0xFF)
            .withArray("arr", arr -> {
                for(int i=0;i<4;i++){
                    arr.add(i*i);
                }
            });
        
        
        byte[] raw = node.toBytes();
        for(byte b:raw){
            if(b >= 32 && b < 256){
                System.out.println((char) b);
            }else{
                System.out.printf("0x%02X\n", b);
            }
        }
        
    }
}
