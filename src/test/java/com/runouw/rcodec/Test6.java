/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.rcodec;

import com.runouw.rcodec.CoderNode;
import java.util.ArrayList;
import org.junit.Test;

/**
 *
 * @author Robert
 */
public class Test6 {

    @Test
    public void test6() {
        /*
        ItemList list = new ItemList();
        list.items.add(Item.generate());
        list.items.add(Item.generate());
        list.items.add(Item.generate());
        
        String first = list.encode(new CoderNode()).withBeautify(BeautifyRules.MINIFIED).toString();
        
        ItemList from = new ItemList();
        from.decode(CoderNode.from(first));
        
        String second = from.encode(new CoderNode()).withBeautify(BeautifyRules.MINIFIED).toString();
        System.out.println(second);
        */
        
        /*
        System.out.println(
                new CoderNode()
                        .set("matrix", new double[][]{
                            new double[]{1, 0, 0, 0},
                            new double[]{0, 1, 0, 0},
                            new double[]{0, 0, 1, 0},
                            new double[]{0, 0, 0, 1}
                        })
                        .toString()
        );
        */
        
        
        ArrayList list = new ArrayList<>();
        list.add(0);
        list.add("test");
        list.add(new double[]{1, 2, 3});
        
        System.out.println(
                new CoderNode()
                        .set("list", list)
                        .toString()
        );
    }
}
