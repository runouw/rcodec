/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.rcodec;

import com.runouw.rcodec.CoderNode;
import com.runouw.rcodec.BeautifyRules;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 *
 * @author Robert
 */
public class Test3 {
    @Test
    public void test3(){
        CoderNode coder2 = new CoderNode()
                .withNode("element", element -> {
                    element.set("frame", 1.0d)
                    .withNode("common", common -> {
                        common.withNode("position", position -> position
                            .set("x", 1.0)
                            .set("y", 2.0)
                        );
                        common.withNode("scale", scale -> scale
                            .set("x", 100.0)
                            .set("y", 100.0)
                        );
                    })
                    .set("speed", "too \\ \" fast")
                    .withArray("arr", arr -> arr
                        .add(1.0)
                        .add(2.0)
                        .addArray(arr2 -> arr2
                            .add(1)
                            .add(2)
                        )
                        .addNode(node -> node
                            .set("x", 1.0)
                            .set("y", 1.0)
                            .set("z", 1.0)
                        )
                    );
                });

        // TODO: confirm it's correct
        System.out.println(coder2.toString(BeautifyRules.MINIFIED));
    }
}
