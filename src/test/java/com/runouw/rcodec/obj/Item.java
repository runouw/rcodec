/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.rcodec.obj;

import com.runouw.rcodec.Codable;
import com.runouw.rcodec.CoderNode;
import java.util.Arrays;
import java.util.Objects;

/**
 *
 * @author Robert
 */
public class Item implements Codable{
    public static boolean USE_GENERIC_ARRAY = false;
    
    private String id = "Item";
    private String[] tags = new String[]{"group1", "group2"};
    private double x = 0;
    private double y = 0;
    
    private double scaleX = 1;
    private double scaleY = 1;
    private double rotation = 0;

    private ColorTransform colors = new ColorTransform();       

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + Objects.hashCode(this.id);
        hash = 83 * hash + Arrays.deepHashCode(this.tags);
        hash = 83 * hash + (int) (Double.doubleToLongBits(this.x) ^ (Double.doubleToLongBits(this.x) >>> 32));
        hash = 83 * hash + (int) (Double.doubleToLongBits(this.y) ^ (Double.doubleToLongBits(this.y) >>> 32));
        hash = 83 * hash + (int) (Double.doubleToLongBits(this.scaleX) ^ (Double.doubleToLongBits(this.scaleX) >>> 32));
        hash = 83 * hash + (int) (Double.doubleToLongBits(this.scaleY) ^ (Double.doubleToLongBits(this.scaleY) >>> 32));
        hash = 83 * hash + (int) (Double.doubleToLongBits(this.rotation) ^ (Double.doubleToLongBits(this.rotation) >>> 32));
        hash = 83 * hash + Objects.hashCode(this.colors);
        return hash;
    }
    
    private static boolean fcmp(double a, double b) {
        return Math.abs(a - b) < 1E-7;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Item other = (Item) obj;
        
        if (!fcmp(this.x, other.x)) {
            return false;
        }
        
        if (!fcmp(this.scaleX, other.scaleX)) {
            return false;
        }
        
        if (!fcmp(this.scaleY, other.scaleY)) {
            return false;
        }
        
        if (!fcmp(this.rotation, other.rotation)) {
            return false;
        }
        
        if (!this.id.equals(other.id)) {
            return false;
        }
                
        if (!Arrays.deepEquals(this.tags, other.tags)) {
            return false;
        }                
        
        return Objects.equals(this.colors, other.colors);
    }
    
    @Override
    public CoderNode encode(CoderNode encoder) {
        if(!USE_GENERIC_ARRAY){
            return encoder
                    .set("id", id)
                    .set("x", x)
                    .set("y", y)
                    .set("scaleX", scaleX)
                    .set("scaleY", scaleY)
                    .set("rotation", rotation)
                    .set("colors", colors)
                    .withArray("tags", arr -> {
                        for (String tag : tags) {
                            arr.add(tag);
                        }
                    });
        }else{
            return encoder
                    .set("id", id)
                    .set("pos", new double[]{x, y})
                    .set("scale", new double[]{scaleX, scaleY})
                    .set("rotation", rotation)
                    .set("colors", colors)
                    .withArray("tags", arr -> {
                        for (String tag : tags) {
                            arr.add(tag);
                        }
                    });
        }
    }

    @Override
    public void decode(CoderNode decoder) {
        decoder
                .ifString("id", val -> id = val)
                .ifDouble("x", val -> x = val)
                .ifDouble("y", val -> y = val)
                .ifDouble("scaleX", val -> scaleX = val)
                .ifDouble("scaleY", val -> scaleY = val)
                .ifDouble("rotation", val -> rotation = val)
                .ifNode("colors", ct -> colors.decode(ct))
                .ifArray("tags", arr -> {
                    tags = new String[arr.size()];
                    for(int i=0;i<arr.size();i++){
                        tags[i] = arr.getString(i, null);
                    }
                });
    }
    
    public static Item generate(){
        Item item = new Item();
        item.x = Math.random() * 9999;
        item.y = Math.random() * 9999;
        item.scaleX = Math.random() * 2 - 1;
        item.scaleY = Math.random() * 2 - 1;
        item.rotation = Math.random() * 360 - 180;
        
        double br = Math.random() * .8 + .2;
        item.colors.multipliers.r = br;
        item.colors.multipliers.g = br;
        item.colors.multipliers.b = br;
        
        item.colors.offset.r = 1 - br;
        item.colors.offset.b = 1 - br;
        item.colors.offset.g = 1 - br;
        
        item.colors.hsl.h = Math.random() * 360;
        
        return item;
    }
    
}
