/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.rcodec.obj;

import com.runouw.rcodec.Codable;
import com.runouw.rcodec.CoderNode;
import java.util.Objects;

/**
 *
 * @author runou
 */
public class ColorTransform implements Codable {

    private static boolean dcmp(final double a, final double b) {
        return Math.abs(a - b) < 1E-7;
    }
    
    public enum Model {
        NORMAL, LEGACY;
    }
    public Model model = Model.NORMAL;

    public static class Multipliers {

        public double r, g, b, a;

        public Multipliers() {
            r = g = b = a = 1;
        }
        
        public boolean equals(final Object obj) {
            if (obj == null) {
                return false;
            }
            
            if (obj instanceof Multipliers) {
                final Multipliers other = (Multipliers) obj;
                
                if (!dcmp(r, other.r)) {
                    return false;
                } else if (!dcmp(g, other.g)) {
                    return false;
                } else if (!dcmp(b, other.b)) {
                    return false;
                } else return dcmp(a, other.a);
            } else {
                return false;
            }
        }
    }
    public Multipliers multipliers = new Multipliers();

    public static class Offset {

        public double r, g, b, a;

        public Offset() {
        }
        
        public boolean equals(final Object obj) {
            if (obj == null) {
                return false;
            }
            
            if (obj instanceof Offset) {
                final Offset other = (Offset) obj;
                
                if (!dcmp(r, other.r)) {
                    return false;
                } else if (!dcmp(g, other.g)) {
                    return false;
                } else if (!dcmp(b, other.b)) {
                    return false;
                } else return dcmp(a, other.a);
            } else {
                return false;
            }
        }
    }
    public Offset offset = new Offset();

    public static class HSL {

        public double h, s, l;

        public HSL() {

        }
        
        public boolean equals(final Object obj) {
            if (obj == null) {
                return false;
            }
            
            if (obj instanceof HSL) {
                final HSL other = (HSL) obj;
                
                if (!dcmp(h, other.h)) {
                    return false;
                } else if (!dcmp(s, other.s)) {
                    return false;
                } else return dcmp(l, other.l);
            } else {
                return false;
            }
        }
    }
    public HSL hsl = new HSL();

    public static class Misc {

        public double br, c;

        public Misc() {
        }
        
        public boolean equals(final Object obj) {
            if (obj == null) {
                return false;
            }
            
            if (obj instanceof Misc) {
                final Misc other = (Misc) obj;
                
                if (!dcmp(br, other.br)) {
                    return false;
                } else return dcmp(c, other.c);
            } else {
                return false;
            }
        }
    }
    public Misc misc = new Misc();

    @Override
    public CoderNode encode(CoderNode encoder) {
        if (!Item.USE_GENERIC_ARRAY) {
            encoder.set("Model", model.toString());
            encoder.withArray("multipliers", arr -> arr
                    .add(multipliers.a)
                    .add(multipliers.r)
                    .add(multipliers.g)
                    .add(multipliers.b)
            );
            encoder.withArray("offset", arr -> arr
                    .add(offset.a)
                    .add(offset.r)
                    .add(offset.g)
                    .add(offset.b)
            );
            encoder.withArray("hsl", arr -> arr
                    .add(hsl.h)
                    .add(hsl.s)
                    .add(hsl.l)
            );
            encoder.withNode("misc", node -> node
                    .set("br", misc.br)
                    .set("c", misc.c)
            );
        } else {
            encoder.set("Model", model.toString());
            encoder.set("multipliers", new double[]{
                multipliers.r,
                multipliers.g,
                multipliers.b,
                multipliers.a
            });
            encoder.set("offset", new double[]{
                offset.r,
                offset.g,
                offset.b,
                offset.a
            });
            encoder.set("hsl", new double[]{
                hsl.h,
                hsl.s,
                hsl.l
            });
            encoder.set("misc", new double[]{
                misc.br,
                misc.c
            });
        }
        return encoder;
    }

    @Override
    public void decode(CoderNode decoder) {
        decoder.withArray("multipliers", arr -> arr
                .ifDouble(0, v -> multipliers.r = v)
                .ifDouble(1, v -> multipliers.g = v)
                .ifDouble(2, v -> multipliers.b = v)
                .ifDouble(3, v -> multipliers.a = v)
        ).withArray("offset", arr -> arr
                .ifDouble(0, v -> offset.r = v)
                .ifDouble(1, v -> offset.g = v)
                .ifDouble(2, v -> offset.b = v)
                .ifDouble(3, v -> offset.a = v)
        ).withArray("hsl", arr -> arr
                .ifDouble(0, v -> hsl.h = v)
                .ifDouble(1, v -> hsl.s = v)
                .ifDouble(2, v -> hsl.l = v)
        ).withNode("misc", node -> node
                .ifDouble("br", v -> misc.br = v)
                .ifDouble("c", v -> misc.c = v)
        );
    }        

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 61 * hash + Objects.hashCode(this.model);
        hash = 61 * hash + Objects.hashCode(this.multipliers);
        hash = 61 * hash + Objects.hashCode(this.offset);
        hash = 61 * hash + Objects.hashCode(this.hsl);
        hash = 61 * hash + Objects.hashCode(this.misc);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ColorTransform other = (ColorTransform) obj;
        
        if (this.model != other.model) {
            return false;
        }
        
        if (!Objects.equals(this.multipliers, other.multipliers)) {
            return false;
        }
        
        if (!Objects.equals(this.offset, other.offset)) {
            return false;
        }
        if (!Objects.equals(this.hsl, other.hsl)) {
            return false;
        }
        
        return Objects.equals(this.misc, other.misc);
    }
}
