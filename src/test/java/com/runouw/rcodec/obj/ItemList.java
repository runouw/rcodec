/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.rcodec.obj;

import com.runouw.rcodec.Codable;
import com.runouw.rcodec.CoderNode;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author Robert
 */
public class ItemList implements Codable {

    public List<Item> items = new ArrayList<>();

    public ItemList() {
    }

    @Override
    public CoderNode encode(CoderNode encoder) {
        encoder.withArray("items", arr -> {
            for (Item item : items) {
                arr.add(item);
            }
        });

        return encoder;
    }

    @Override
    public void decode(CoderNode decoder) {
        items.clear();

        decoder.withArray("items", arr -> {
            for (int i = 0; i < arr.size(); i++) {
                arr.getNode(i).ifPresent(node -> {
                    Item newItem = new Item();
                    newItem.decode(node);
                    items.add(newItem);
                });
            }
        });
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + Objects.hashCode(this.items);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        
        if (obj == null) {
            return false;
        }
        
        if (obj instanceof ItemList) {
            final ItemList other = (ItemList) obj;
            
            final int mySize = this.items.size();
            final int theirSize = other.items.size();
            
            if (mySize != theirSize) {
                return false;
            }
            
            for (int i = 0; i < mySize; i++) {
                if (!this.items.get(i).equals(other.items.get(i))) {
                    return false;
                }
            }
            
            return true;
        } else {
            return false;
        }
    }
}
