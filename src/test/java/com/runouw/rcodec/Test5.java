/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.rcodec;

import com.runouw.rcodec.CoderNode;
import com.runouw.rcodec.BeautifyRules;
import com.runouw.rcodec.obj.ColorTransform;
import java.nio.file.Paths;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 *
 * @author runou
 */
public class Test5 {
    @Test
    public void Test1() throws Exception{
        ColorTransform ct = new ColorTransform();

        CoderNode node = new CoderNode();
        node.set("colors", ct);

        // System.out.println(node.withBeautify(new BeautifyRules(Indentation.SPACES, 4)));
        
        node.withBeautify(BeautifyRules.MINIFIED).writeToFile(Paths.get("test5.json-min"));
        node.withBeautify(BeautifyRules.SPACES_X_4).writeToFile(Paths.get("test5.json"));
        node.writeBJSONFile(Paths.get("test5.bjson"));

        CoderNode fromMinified = new CoderNode().fromPath(Paths.get("test5.json-min"));
        CoderNode fromJSON = new CoderNode().fromPath(Paths.get("test5.json"));
        CoderNode fromBJSON = new CoderNode().fromPath(Paths.get("test5.bjson"));

        assertEquals(node.toMinifiedString(), fromMinified.toMinifiedString());
        assertEquals(node.toMinifiedString(), fromJSON.toMinifiedString());
        assertEquals(node.toMinifiedString(), fromBJSON.toMinifiedString());
        
        assertEquals(node, fromMinified);
        assertEquals(node, fromJSON);
        assertEquals(node, fromBJSON);
    }
}
