/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.hypercodec;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import org.junit.Test;

/**
 *
 * @author zmichaels
 */
public class StrTest {
    @Test
    public void testStr() {
        final ByteBuffer data = Charset.forName("UTF8").encode("Test");
        
        int i = 0;
        while(data.hasRemaining()) {
            System.out.printf("[%d]: %d%n", i, data.get());
        }
    }
}
