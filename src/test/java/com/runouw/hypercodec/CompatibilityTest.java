/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.hypercodec;

import com.runouw.rcodec.CoderNode;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Paths;
import java.util.Map;
import junit.framework.Assert;
import org.junit.Test;

/**
 *
 * @author zmichaels
 */
public class CompatibilityTest {        
    // disabled since hypercodec uses Fixed-point decimals and I don't have a means to unwrap to Fixed Point yet...
    //@Test
    public void testJSONHypercodecCompatibility() throws IOException {
        final Map<String, Object> rcodec = CoderNode.unwrap(new CoderNode().fromPath(Paths.get("test5.json")));
        final Map<String, Object> hypercodec = JSONDecoder.decode(Paths.get("test5.json"), Charset.forName("UTF8"));
        
        Assert.assertEquals("JSON Decode -> Unwrap failed!", rcodec.toString(), hypercodec.toString());
    }
    
    @Test
    public void testBJSONHypercodecCompatibility() throws IOException {
        final Map<String, Object> rcodec = CoderNode.unwrap(new CoderNode().fromPath(Paths.get("test5.bjson")));
        final Map<String, Object> hypercodec = BJSONDecoder.decode(Paths.get("test5.bjson"));
        
        Assert.assertEquals("BJSON Decode -> Unwrap failed!", rcodec.toString(), hypercodec.toString());
    }
    
    @Test
    public void testJSONRCodecCompatibility() throws IOException {
        final CoderNode rcodec = new CoderNode().fromPath(Paths.get("test5.json"));
        final CoderNode hypercodec = CoderNode.decode(Paths.get("test5.json"), Charset.forName("UTF8"));
        
        Assert.assertEquals("JSON Decode -> Encode failed!", hypercodec.toString(), rcodec.toString());
    }
    
    @Test
    public void testBJSONRCodecCompatibility() throws IOException {
        final CoderNode rcodec = new CoderNode().fromPath(Paths.get("test5.bjson"));
        final CoderNode hypercodec = CoderNode.decode(Paths.get("test5.bjson"), Charset.forName("UTF8"));
        
        Assert.assertEquals("BJSON Decode -> JSON Encode failed!", hypercodec.toString(), rcodec.toString());
    }
}
