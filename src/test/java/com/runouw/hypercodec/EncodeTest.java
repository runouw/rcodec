/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.hypercodec;

import java.io.IOException;
import java.nio.CharBuffer;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import org.junit.Test;

/**
 *
 * @author zmichaels
 */
public class EncodeTest {
    @Test
    public void testEncode() throws IOException {
        final Map<String, Object> json = new HashMap<>();
        
        json.put("Message", "Hello World!");
        json.put("Scouter Level", 9001L);
        json.put("Pi", 3.1415);
        json.put("-Pi", -3.1415);
        json.put("Sandwich", Arrays.asList("Bread", "Cheese", "Ham"));
        json.put("Escape Test", "\"String\"");
        
        CharBuffer out = CharBuffer.allocate(4 * 4096);
        
        JSONEncoder.encode(out, json);
        
        out.flip();
        System.out.println(out.toString());
    }
}
