/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.hypercodec;

import com.runouw.rcodec.CoderNode;
import java.io.IOException;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.file.Paths;
import java.util.Map;
import org.junit.Test;

/**
 *
 * @author zmichaels
 */
public class DecodeTest {

    private static final String SIMPLE_JSON = "{\"Message\" : \"Hello World!\" , \"Escaped Text\" : \"\\\"String\\\"\", \"Scouter Level\": 9001, \"Pi\": 3.1415, \"-Pi\"  : -3.1415, \"Sandwich\" : [\"Bread\", \"Ham\", \"Cheese\"]}";

    @Test
    public void testDecode() throws IOException {
        final Map<String, Object> obj = JSONDecoder.decode(CharBuffer.wrap(SIMPLE_JSON));

        System.out.println(obj);
    }            
    
    //@Test
    public void testDecodeToRCodec() throws IOException {
        final Map<String, Object> obj = BJSONDecoder.decode(Paths.get("benchmark.bjson"));
        final CoderNode wrapped = CoderNode.wrap(obj);
        
        System.out.println(wrapped);
    }

    @Test
    public void testDecodeComplex() throws IOException {
        final Map<String, Object> obj = JSONDecoder.decode(Paths.get("src/test/resources/sample.json"), Charset.forName("UTF8"));
        
        //System.out.println(obj);
        System.out.println(CoderNode.wrap(obj));
    }
}
