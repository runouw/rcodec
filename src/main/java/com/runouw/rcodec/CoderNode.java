/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.rcodec;

import com.runouw.hypercodec.BJSONDecoder;
import com.runouw.hypercodec.JSONDecoder;
import com.runouw.rcodec.DecodeException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * An ordered Key -> Value map for encodable and decodable data.
 * @author rhewitt - Originator
 * @author zmichaels - Maintainer, Documentation 
 */
public class CoderNode implements CoderContainer<String, CoderNode>{
    private final Map<String, CoderData> map = new LinkedHashMap<>();
    
    private CoderSettings settings;

    public CoderNode(CoderSettings settings) {
        this.settings = settings;
    }
    
    public static Map<String, Object> unwrap(final CoderNode node) throws IOException {
        final Map<String, Object> out = new LinkedHashMap<>();
        
        for (Map.Entry<String, CoderData> pair : node.map.entrySet()) {
            out.put(pair.getKey(), CoderData.unwrap(pair.getValue()));
        }
        
        return out;
    }
    
    public static CoderNode wrap(final Map<String, Object> obj) throws IOException {
        final CoderNode out = new CoderNode();
        
        for (Map.Entry<String, Object> pair : obj.entrySet()) {
            out.map.put(pair.getKey(), CoderData.wrap(pair.getValue()));
        }
        
        return out;
    }
    
    /**
     * Decodes the file using hypercodec backend.
     * @param path the Path to the file.
     * @param charset the Charset to use. UTF8 is recommended.
     * @return the translated CoderNode.
     * @throws IOException if the file could not be read properly or the data could not be wrapped successfully.
     */
    public static CoderNode decode(final Path path, final Charset charset) throws IOException {        
        try (FileChannel fc = FileChannel.open(path, StandardOpenOption.READ)) {
            final ByteBuffer data = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());                                    
            final Map<String, Object> obj;
            
            if (BJSONDecoder.checkHeader(data)) {
                obj = BJSONDecoder.decode(data);
            } else {                
                obj = JSONDecoder.decode(charset.decode(data));
            }
            
            return CoderNode.wrap(obj);
        }
    }        
    
    public CoderNode() {
        this(new CoderSettings());
    }
    
    public void setSettings(CoderSettings settings) {
        this.settings = settings;
    }

    @Override
    public CoderSettings getSettings() {
        return settings;
    }
    
    @Override
    public CoderNode self() {
        return this;
    }

    @Override
    public CoderData getCoderData(String key) {
        CoderData value = map.get(key);
        if (value == null) {
            return new CoderData(CoderData.Type.NULL, null);
        } else {
            return value;
        }
    }

    @Override
    public CoderNode setCoderData(String key, CoderData data) {
        map.put(key, data);        

        return this;
    }

    @Override
    public boolean contains(String key) {
        return map.containsKey(key);
    }
    
    public List<String> getKeys() {
        return Collections.unmodifiableList(new ArrayList<>(map.keySet()));
    }

    @Override
    public String toString() {
        return asString();
    }

    @Override
    public JSONWriter write(JSONWriter writer) {
        writer.append('{');
        writer.setLevel(writer.getLevel() + 1);

        writer.putNewline(writer.getLevel());

        boolean useDelimiter = false;
        
        for (Map.Entry<String, CoderData> entry : map.entrySet()) {
            if (useDelimiter) {
                writer.putDelimiter().putNewline(writer.getLevel());
            }
            
            writer.appendQuotedAddSlashes(entry.getKey());
            writer.putColon();
            
            entry.getValue().write(writer);
            
            useDelimiter = true;
        }

        writer.setLevel(writer.getLevel() - 1);
        writer.putNewline(writer.getLevel());
        writer.append('}');
        return writer;
    }

    @Override
    public ByteWriter write(ByteWriter writer) {
        writer.write((byte) '{');
        
        for (Map.Entry<String, CoderData> entry : map.entrySet()) {
            writer.writeString(entry.getKey());
            writer.write((byte) '\0');
            entry.getValue().write(writer);
        }                
        
        // null string needed to signal an end to the stream of key value pairs
        writer.write((byte) '\0');
        writer.write((byte) '}');

        return writer;
    }

    @Override
    public CoderNode read(JSONReader reader, boolean readHeader) {
        if(readHeader && reader.getNextToken().type != JSONTokenizer.TokenType.START_BRACE){
            throw new DecodeException("Start token does not match!");
        }
        
        boolean expectComma = false;
        while(true){
            JSONTokenizer.Token token = reader.getNextToken();
            switch (token.type) {
                case COMMA:
                    if(!expectComma){
                        throw new DecodeException("Unexpected token " + token);
                    }
                    expectComma = false;
                    
                    break;
                case END_BRACE:
                    return this;
                case EOF:
                    throw new DecodeException("End of file reached!");
                case STRING:
                    String key = token.buf;

                    JSONTokenizer.Token colonToken = reader.getNextToken();
                    if(colonToken.type != JSONTokenizer.TokenType.COLON){
                        throw new DecodeException("Unexpected token (expecting ':') " + colonToken);
                    }

                    JSONTokenizer.Token valueToken = reader.getNextToken();
                    
                    CoderData data = CoderData.fromJSONToken(valueToken, reader);
                    map.put(key, data);
                    
                    expectComma = true;
                    break;
                default:
                    throw new DecodeException("Unexpected token " + token);
            }
        }
    }
    
    @Override
    public CoderNode read(ByteReader reader, boolean readHeader) {
        if(readHeader && reader.readByte() != '{'){
            throw new DecodeException("Byte header for CoderNode not found!");
        }
        
        while(true){
            String key = reader.readString();
            
            byte c = reader.readByte();
            if(c == '}'){
                return this;
            }
            CoderData.Type type = CoderData.Type.fromByte(c)
                    .orElseThrow(() -> new DecodeException("ByteReader character not supported! " + c));
            CoderData data = type.decode(reader);
            
            map.put(key, data);
        }
    }
    
    /*
    public static CoderNode from(byte[] bytes){
        ByteReaderByteArray reader = new ByteReaderByteArray(bytes);
        if(reader.checkHeader()){
            return new CoderNode().read(reader);
        }else{
            JSONReader jsonReader = new JSONReaderString(bytes);
            
            return new CoderNode().read(jsonReader, true);
        }
    }
    
    public static CoderNode from(String str){
        JSONReader jsonReader = new JSONReaderString(str);
            
        return new CoderNode().read(jsonReader, true);
    }
    */

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 43 * hash + Objects.hashCode(this.map);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CoderNode other = (CoderNode) obj;
        
        return Objects.equals(this.map.entrySet(), other.map.entrySet());
    }
    
    
}
