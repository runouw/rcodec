/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.rcodec;

/**
 * Supported types of indentation that a JSON formatted string can have.
 * @author rhewitt - Originator
 * @author zmichaels - Maintainer, Documentation 
 */
public enum Indentation {
    TABS, SPACES, MINIFIED;
}
