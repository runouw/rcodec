/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.rcodec;

import java.nio.CharBuffer;

/**
 * Simple Fixed-Point decimal object. Useful for lossless textual precision.
 * Fixed-point math represents a decimal by multiplying the decimal by a scale
 * such that it becomes a whole number. Conversion back to decimal requires
 * division by the scale.
 *
 * Fixed guarantees that String -> Fixed -> String serialization results in no
 * data loss. There may be data loss if the data type is translated to some
 * other format.
 *
 * @author zmichaels
 */
public final class Fixed extends Number {

    public final long value;
    public final long scale;

    /**
     * Constructs a new Fixed object using the given value and scale.
     *
     * @param value The value. This is
     * @param scale
     */
    public Fixed(final long value, final long scale) {
        this.value = value;
        this.scale = scale;
    }

    /**
     * Converts a double to a Fixed-point decimal. A constant scale of 2048 will
     * be used.
     *
     * @param value the double value.
     */
    public Fixed(final double value) {
        this.scale = 1 << 11;
        this.value = (long) (value * this.scale);
    }

    private static double decodeDouble(final CharBuffer data) throws NumberFormatException {
        int offset = 0;

        data.mark();

        while (data.hasRemaining()) {
            switch (data.get()) {
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                case '-':
                case '.':
                case 'e':
                case 'E':
                    offset++;
                    break;
                default:
                    data.reset();

                    final CharBuffer subData = data.slice();

                    data.position(data.position() + offset);

                    subData.limit(offset);

                    return Double.parseDouble(subData.toString());
            }
        }

        throw new NumberFormatException("Malformed Double!");
    }

    /**
     * Decodes a Fixed object by scanning a CharBuffer. The end of the Fixed
     * object is determined by an interruption in valid characters or end of
     * buffer.
     *
     * All chars read will be consumed. This method overwrites the buffer mark.
     *
     * @param strval the CharBuffer.
     * @return the Fixed Object.
     * @throws NumberFormatException
     */
    public static Fixed decodeFixed(final CharBuffer strval) throws NumberFormatException {
        boolean isNegative = false;
        long intPart = 0;
        long value = 0;
        int digits = 0;
        boolean hasDecimal = false;
        boolean isFixed = false;

        strval.mark();

        decode_loop:
        while (strval.hasRemaining()) {
            final char lookup = strval.get();

            switch (lookup) {
                case '-':
                    isNegative = true;
                    break;
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                    value = value * 10 + (long) (lookup - '0');
                    digits++;
                    break;
                case '/':
                    intPart = value;
                    value = 0;
                    isFixed = true;
                    break;
                case '.':
                    intPart = value;
                    value = 0;
                    digits = 0;
                    hasDecimal = true;
                    break;
                case 'e':
                case 'E':
                    strval.reset();
                    return new Fixed(decodeDouble(strval));
                default:
                    strval.position(strval.position() - 1);
                    break decode_loop; // dark java magicks to jump out 2 layers
            }
        }

        if (isFixed) {
            return isNegative ? new Fixed(-intPart, value) : new Fixed(intPart, value);
        } else if (hasDecimal) {
            long scale = 1;

            for (int i = 0; i < digits; i++) {
                scale *= 10;
            }

            return isNegative ? new Fixed(-intPart * scale - value, scale) : new Fixed(intPart * scale + value, scale);
        } else {
            return isNegative ? new Fixed(-value, 1) : new Fixed(value, 1);
        }
    }

    /**
     * Parses a Fixed point value from a String. Supported notations are either
     * in ddddd.ddddd or dddd/dddd. The later is the preferred format.
     *
     * Using exponential form will force use of Double.parseDouble. Using
     * multiple negative signs, decimal points, or exponents is undefined
     * behavior.
     *
     * @param strval the String to parse
     * @return the Fixed object
     * @throws NumberFormatException if there was an error in decoding the
     * number.
     */
    public static Fixed parseFixed(final String strval) throws NumberFormatException {
        final char[] carr = strval.toCharArray();

        boolean isNegative = false;
        long intPart = 0;
        long value = 0;
        int digits = 0;
        boolean hasDecimal = false;
        boolean isFixed = false;

        for (int i = 0; i < carr.length; i++) {
            switch (carr[i]) {
                case '-':
                    isNegative = true;
                    break;
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                    value = value * 10 + (long) (carr[i] - '0');
                    digits++;
                    break;
                case '/':
                    intPart = value;
                    value = 0;
                    isFixed = true;
                    break;
                case '.':
                    intPart = value;
                    value = 0;
                    digits = 0;
                    hasDecimal = true;
                    break;
                case 'e':
                case 'E':
                    return new Fixed(Double.parseDouble(strval));
                default:
                    throw new NumberFormatException("Invalid character: " + carr[i]);
            }
        }

        if (digits > 15) {
            throw new NumberFormatException("Too many digits!");
        }

        if (isFixed) {
            return isNegative ? new Fixed(-intPart, value) : new Fixed(intPart, value);
        } else if (hasDecimal) {
            long scale = 1;

            for (int i = 0; i < digits; i++) {
                scale *= 10;
            }

            return isNegative ? new Fixed(-intPart * scale - value, scale) : new Fixed(intPart * scale + value, scale);
        } else {
            return isNegative ? new Fixed(-value, 1) : new Fixed(value, 1);
        }
    }

    @Override
    public double doubleValue() {
        return ((double) this.value) / this.scale;
    }

    @Override
    public int intValue() {
        return (int) doubleValue();
    }

    /**
     * Checks if the Fixed point number is actually an integer.
     * @return true if scale == 1.
     */
    public boolean isInteger() {
        return this.scale == 1;
    }

    @Override
    public long longValue() {
        return this.isInteger() ? this.value : (long) this.doubleValue();
    }

    @Override
    public float floatValue() {
        return (float) doubleValue();
    }

    @Override
    public String toString() {
        return toString(this);
    }

    /**
     * Converts a Fixed type object to String.
     * @param fixed the Fixed point object.
     * @return the String representation of it.
     */
    public static String toString(final Fixed fixed) {
        if (fixed.isInteger()) {
            return Long.toString(fixed.value);
        } else {
            return Long.toString(fixed.value) + "/" + Long.toString(fixed.scale);
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        } else if (obj instanceof Fixed) {
            final Fixed other = (Fixed) obj;

            if (this.scale == other.scale && this.value == other.value) {
                return true;

                // try a little harder anyway since there are multiple representations of the same number
            } else {
                return Double.compare(this.doubleValue(), other.doubleValue()) == 0;
            }
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + (int) (this.value ^ (this.value >>> 32));
        hash = 71 * hash + (int) (this.scale ^ (this.scale >>> 32));
        return hash;
    }
}
