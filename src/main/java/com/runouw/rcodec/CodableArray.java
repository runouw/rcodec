/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.rcodec;

/**
 * An interface that marks an object to be serializable to a JSON/BJSON array
 * @author rhewitt - Originator
 * @author zmichaels - Maintainer, Documentation 
 */
public interface CodableArray extends CodableArrayEncode, CodableArrayDecode{
    
}
