/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.hypercodec;

import com.runouw.rcodec.Fixed;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author zmichaels
 */
public final class BJSONDecoder {

    private static final Charset DEFAULT_CHARSET = Charset.forName("UTF8");

    private static String decodeStringSafe(final ByteBuffer data) throws IOException {
        int offset = 0;

        data.mark();

        while (data.hasRemaining()) {
            if (data.get() == '\0') {
                data.reset();

                final ByteBuffer subBuffer = data.slice();

                // set position to AFTER the null-terminator
                data.position(data.position() + offset + 1);

                subBuffer.limit(offset);

                return DEFAULT_CHARSET.decode(subBuffer).toString();
            } else {
                offset++;
            }
        }

        throw new IOException("String is not null-terminated!");
    }

    private static ByteBuffer decodeString(final ByteBuffer data) throws IOException {
        int offset = 0;

        data.mark();

        while (data.hasRemaining()) {
            if (data.get() == '\0') {
                data.reset();

                final ByteBuffer subBuffer = data.slice();

                // set position to AFTER the null-terminator
                data.position(data.position() + offset + 1);

                subBuffer.limit(offset);

                return subBuffer;
            } else {
                offset++;
            }
        }

        throw new IOException("String is not null-terminated!");
    }

    private static Map<String, Object> decodeObjectSafe(final ByteBuffer data) throws IOException {
        final Map<String, Object> out = new LinkedHashMap<>();

        while (data.hasRemaining()) {
            data.mark();
            
            if (data.get() == '\0' && data.get() == '}') {
                return out;
            } else {
                data.reset();
            }

            final String key = decodeStringSafe(data);
            final Object value = decodeAnySafe(data);

            out.put(key, value);
        }

        throw new IOException("Malformed Object!");
    }

    private static List<Object> decodeArraySafe(final ByteBuffer data) throws IOException {
        final List<Object> out = new ArrayList<>();

        while (data.hasRemaining()) {
            data.mark();

            if (data.get() == ']') {
                return out;
            } else {
                data.reset();
                out.add(decodeAnySafe(data));
            }
        }

        throw new IOException("Malformed Array!");
    }

    private static byte[] decodeBlobSafe(final ByteBuffer data) {
        final int size = data.getInt();
        final byte[] out = new byte[size];

        data.get(out);

        return out;
    }

    private static Object decodeAnySafe(final ByteBuffer data) throws IOException {
        final byte lookup = data.get();

        switch (lookup) {
            case 's':
                return decodeStringSafe(data);
            case 'i':
                return data.getInt();
            case '2':
                return data.getShort();
            case 'b':
                return data.get() != 0;
            case '1':
                return data.get();
            case 'l':
                return data.getLong();
            case 'f':
                return data.getFloat();
            case 'd':
                return data.getDouble();
            case 'x':
                return new Fixed(data.getLong(), data.getLong());
            case '{':
                return decodeObjectSafe(data);
            case '[':
                return decodeArraySafe(data);
            case '!':
                return decodeBlobSafe(data);
            case '_':
                return null;
            default:
                throw new IOException("Unsupported token: " + (char) lookup);
        }
    }

    /**
     * Checks if the blob contains BJSON. All read operations will be undone if
     * the blob does not start with the BJSON header.
     *
     * @param data the blob of data to parse.
     * @return true if the blob is BJSON. The header will be removed if true.     
     */
    public static boolean checkHeader(final ByteBuffer data) {
        data.mark();

        if (data.get() != 'B') {
            data.reset();
            return false;
        }

        if (data.get() != 'J') {
            data.reset();
            return false;
        }

        if (data.get() != 'S') {
            data.reset();
            return false;
        }

        if (data.get() != 'O') {
            data.reset();
            return false;
        }

        if (data.get() != 'N') {
            data.reset();
            return false;
        }

        return true;
    }

    /**
     * Decodes a block of data as BJSON. The BJSON header should already be
     * stripped from the data blob.
     *
     * @param data the data blob to parse.
     * @return the structured object.
     * @throws IOException if the blob could not be parsed.
     */
    public static Map<String, Object> decode(final ByteBuffer data) throws IOException {
        final Object decodedData = decodeAnySafe(data);

        if (decodedData instanceof Map) {
            return (Map<String, Object>) decodedData;
        } else {
            throw new IOException("Malformed BJSON! Expected type: Map, got: " + decodedData.getClass().getSimpleName());
        }
    }

    /**
     * Decodes a BJSON file into a Map structure.
     *
     * @param path the Path for the file.
     * @return the structured object.
     * @throws IOException if the file is not BJSON or if the file could not be
     * read.
     */
    public static Map<String, Object> decode(final Path path) throws IOException {
        try (final FileChannel fc = FileChannel.open(path, StandardOpenOption.READ)) {
            final ByteBuffer data = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());

            if (checkHeader(data)) {
                return decode(data);
            } else {
                throw new IOException("File is not BJSON!");
            }
        }
    }
}
