/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.hypercodec;

import com.runouw.rcodec.Fixed;
import java.io.IOException;
import java.nio.CharBuffer;
import java.util.List;
import java.util.Map;

/**
 *
 * @author zmichaels
 */
public final class JSONEncoder {

    private static void encodeStringSafe(final CharBuffer data, final String str) {
        data.put('\"');
        
        final char[] chars = str.toCharArray();
        
        for (int i = 0; i < chars.length; i++) {
            switch (chars[i]) {
                case '\\':
                    data.put("\\\\");
                    break;
                case '\"':
                    data.put("\\\"");
                    break;
                case '\n':
                    data.put("\\\n");
                    break;
                case '\t':
                    data.put("\\\t");
                    break;
                case '\r':
                    data.put("\\\r");
                    break;
                default:
                    data.put(chars[i]);
            }
        }
        
        data.put('\"');
    }
    
    private static void encodeFixedSafe(final CharBuffer data, final Fixed value) {
        data.put(Fixed.toString(value));
    }

    private static void encodeDoubleSafe(final CharBuffer data, final double value) {
        data.put(Double.toString(value));
    }

    private static void encodeLongSafe(final CharBuffer data, final long value) {
        data.put(Long.toString(value));
    }

    private static void encodeBooleanSafe(final CharBuffer data, final boolean value) {
        data.put(Boolean.toString(value));
    }

    private static void encodeNullSafe(final CharBuffer data) {
        data.put("null");
    }

    private static void encodeArraySafe(final CharBuffer data, final List<Object> array) throws IOException {
        data.put('[');

        if (!array.isEmpty()) {
            for (int i = 0; i < array.size() - 1; i++) {
                encodeAnySafe(data, array.get(i));
                data.put(',');
            }
            
            encodeAnySafe(data, array.get(array.size() - 1));
        }

        data.put(']');
    }

    private static void encodeObjectSafe(final CharBuffer data, final Map<String, Object> obj) throws IOException {
        data.put('{');

        int i = 0;
        for (Map.Entry<String, Object> pair : obj.entrySet()) {
            encodeStringSafe(data, pair.getKey());
            data.put(':');
            encodeAnySafe(data, pair.getValue());
            
            if (i < obj.size() - 1) {
                data.put(',');
            }
            
            i++;
        }

        data.put('}');
    }

    @SuppressWarnings("unchecked")
    private static void encodeAnySafe(final CharBuffer data, final Object obj) throws IOException {
        if (obj == null) {
            encodeNullSafe(data);
        } else if (obj instanceof Map) {
            encodeObjectSafe(data, (Map<String, Object>) obj);
        } else if (obj instanceof List) {
            encodeArraySafe(data, (List<Object>) obj);
        } else if (obj instanceof String) {
            encodeStringSafe(data, (String) obj);
        } else if (obj instanceof Fixed) {
            encodeFixedSafe(data, (Fixed) obj);
        } else if (obj instanceof Double) {
            encodeDoubleSafe(data, (Double) obj);
        } else if (obj instanceof Long) {
            encodeLongSafe(data, (Long) obj);
        } else if (obj instanceof Boolean) {
            encodeBooleanSafe(data, (Boolean) obj);
        } else if (obj instanceof Integer) {
            encodeLongSafe(data, (Integer) obj);
        } else if (obj instanceof Short) {
            encodeLongSafe(data, (Short) obj);
        } else if (obj instanceof Byte) {
            encodeLongSafe(data, (Byte) obj);
        } else if (obj instanceof Float) {
            encodeDoubleSafe(data, (Float) obj);
        } else {
            throw new IOException("Unsupported object type: " + obj.getClass().getSimpleName());
        }
    }

    public static void encode(final CharBuffer output, final Map<String, Object> map) throws IOException {
        encodeObjectSafe(output, map);        
    }
}
