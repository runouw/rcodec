/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.hypercodec;

import com.runouw.rcodec.Fixed;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author zmichaels
 */
public final class JSONDecoder {

    private static double decodeDouble(final CharBuffer data) throws IOException {
        int offset = 0;

        data.mark();

        while (data.hasRemaining()) {
            switch (data.get()) {
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                case '-':
                case '.':
                case 'e':
                case 'E':
                    offset++;
                    break;
                default:
                    data.reset();

                    final CharBuffer subBuf = data.slice();

                    data.position(data.position() + offset);

                    subBuf.limit(offset);

                    return Double.parseDouble(subBuf.toString());
            }
        }

        throw new IOException("Malformed Double!");
    }

    private static Object decodeNumeric(final CharBuffer data) throws IOException {
        data.mark();
        long value = 0;
        long intPart = 0;
        int digits = 0;
        boolean isInteger = true;
        boolean isPositive = true;

        while (data.hasRemaining()) {
            final char lookup = data.get();

            switch (lookup) {
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                    value = value * 10L + (long) (lookup - '0');
                    digits++;
                    break;
                case '-':
                    isPositive = false;
                    break;
                case '.':
                    intPart = value;
                    value = 0;
                    digits = 0;
                    isInteger = false;
                    break;
                case 'e':
                case 'E': {
                    // this is literally the worst case scenario.
                    data.reset();
                    return decodeDouble(data);
                }
                default:
                    data.position(data.position() - 1); //unread the last byte

                    if (isInteger) {
                        return isPositive ? value : -value;
                    } else {
                        double divisor = 1.0;

                        for (int i = 0; i < digits; i++) {
                            divisor *= 10;
                        }

                        final double decValue = intPart + (value / divisor);

                        return isPositive ? decValue : -decValue;
                    }
            }
        }

        throw new IOException("Malformed Numeric!");
    }

    
    private static String decodeEscapeString(final CharBuffer data) throws IOException {
        final StringBuilder out = new StringBuilder();

        while (data.hasRemaining()) {
            final char lookup = data.get();

            switch (lookup) {
                case '\\':
                    switch (data.get()) {
                        case '\"':                            
                            out.append('\"');
                            break;
                        case '\\':
                            out.append('\\');
                            break;
                        case '/':
                            out.append('/');
                            break;
                        case 'b':
                            out.append('\b');
                            break;
                        case 'f':
                            out.append('\f');
                            break;
                        case 'n':
                            out.append('\n');
                            break;
                        case 'r':
                            out.append('\r');
                            break;
                        case 't':
                            out.append('\t');
                            break;
                        case 'u':                            
                            final char[] uHex = new char[4];
                            
                            data.get(uHex);
                            
                            final int uVal = Integer.parseInt(new String(uHex), 16);
                            
                            out.append((char) uVal);
                            break;
                        default:
                            throw new IOException("Unexpected escaped character: " + data.get(data.position() - 1));
                    }
                    break;
                case '\"':                    
                    return out.toString();
                default:
                    out.append(lookup);
                    break;
            }
        }

        throw new IOException("Unclosed String!");
    }

    private static String decodeString(final CharBuffer data) throws IOException {
        data.get(); // dispose the first byte. Its the open quote.
        data.mark();

        int offset = 0;

        while (data.hasRemaining()) {
            switch (data.get()) {
                // switch decoding operation for strings with escape characters
                // most strings will not have escape characters, so go fast when possible.
                case '\\':
                    data.reset();
                    return decodeEscapeString(data);
                case '\"':
                    data.reset();

                    final CharBuffer subBuffer = data.slice();

                    // set the position to AFTER the end quote
                    data.position(data.position() + offset + 1);

                    subBuffer.limit(offset);

                    return subBuffer.toString();
                default:
                    offset++;
                    break;
            }
        }

        throw new IOException("Unclosed String!");
    }

    private static boolean isWhitespace(final char lookup) {
        switch (lookup) {
            case ' ':
            case '\t':
            case '\r':
            case '\n':
                return true;
            default:
                return false;
        }
    }

    private static boolean decodeFalse(final CharBuffer data) throws IOException {
        char c = data.get();

        if (c != 'f' && c != 'F') {
            throw new IOException("Malformed boolean: expected \"f\"");
        }

        c = data.get();

        if (c != 'a' && c != 'A') {
            throw new IOException("Malformed boolean: expected \"fa\"");
        }

        c = data.get();

        if (c != 'l' && c != 'L') {
            throw new IOException("Malformed boolean: expected \"fal\"");
        }

        c = data.get();

        if (c != 's' && c != 'S') {
            throw new IOException("Malformed boolean: expected \"fals\"");
        }

        c = data.get();

        if (c != 'e' && c != 'E') {
            throw new IOException("Malformed boolean: expected \"false\"");
        }

        return false;
    }

    private static boolean decodeTrue(final CharBuffer data) throws IOException {
        char c = data.get();

        if (c != 't' && c != 'T') {
            throw new IOException("Malformed boolean: expected \"t\"");
        }

        c = data.get();

        if (c != 'r' && c != 'R') {
            throw new IOException("Malformed boolean: expected \"tr\"");
        }

        c = data.get();

        if (c != 'u' && c != 'U') {
            throw new IOException("Malformed boolean: expected \"tru\"");
        }

        c = data.get();

        if (c != 'e' && c != 'E') {
            throw new IOException("Malformed boolean: expected \"true\"");
        }

        return true;
    }

    private static void skipWhitespace(final CharBuffer data) {
        while (true) {
            data.mark();
            if (!isWhitespace(data.get())) {
                data.reset();
                return;
            }
        }
    }

    private static List<Object> decodeArray(final CharBuffer data) throws IOException {
        data.get(); // dispose of first byte
        final List<Object> out = new ArrayList<>();

        while (data.hasRemaining()) {
            skipWhitespace(data);

            data.mark();

            switch (data.get()) {
                case ']':
                    return out;
                case ',':
                    break;
                default:
                    data.reset();
                    out.add(decodeAny(data));
            }
        }

        throw new IOException("Malformed array!");
    }

    private static Map<String, Object> decodeObject(final CharBuffer data) throws IOException {
        data.get(); // dispose of first char

        final Map<String, Object> out = new LinkedHashMap<>();

        while (data.hasRemaining()) {
            skipWhitespace(data);

            data.mark();

            if (data.get() == '}') {
                return out;
            } else {
                data.reset();
            }

            skipWhitespace(data);

            final String key = decodeString(data);

            skipWhitespace(data);

            if (data.get() != ':') {
                throw new IOException("Malformed Object! Expected \":\" but got \"" + data.get(data.position() - 1) + "\"");
            }

            skipWhitespace(data);

            final Object value = decodeAny(data);

            out.put(key, value);

            skipWhitespace(data);

            switch (data.get()) {
                case '}':
                    return out;
                case ',':
                    break;
                default:
                    throw new IOException("Malformed Object! Expected \",\" or \"}\" but got \"" + data.get(data.position() - 1) + "\"");
            }
        }

        throw new IOException("Malformed Object!");
    }

    private static Object decodeNull(final CharBuffer data) throws IOException {
        char c = data.get();

        if (c != 'n' && c != 'N') {
            throw new IOException("Malformed Null! Expected: \"n\" got: \"" + c + "\"");
        }

        c = data.get();

        if (c != 'u' && c != 'U') {
            throw new IOException("Malformed Null! Expected: \"nu\" got: \"n" + c + "\"");
        }

        c = data.get();

        if (c != 'l' && c != 'L') {
            throw new IOException("Malformed null! Expected: \"nul\" got: \"nu" + c + "\"");
        }

        c = data.get();

        if (c != 'l' && c != 'L') {
            throw new IOException("Malformed null! Expected \"null\" got: \"nul" + c + "\"");
        }

        return null;
    }

    private static Object decodeAny(final CharBuffer data) throws IOException {
        final char lookup = data.get(data.position());

        switch (lookup) {
            case '\"':
                return decodeString(data);
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
            case '-':
                return Fixed.decodeFixed(data);
            case '[':
                return decodeArray(data);
            case '{':
                return decodeObject(data);
            case 't':
            case 'T':
                return decodeTrue(data);
            case 'f':
            case 'F':
                return decodeFalse(data);
            case ' ':
            case '\t':
            case '\n':
            case '\r':
                skipWhitespace(data);
                return decodeAny(data);
            case 'n':
                return decodeNull(data);
            default:
                throw new IOException("Malformed JSON!");
        }
    }

    @SuppressWarnings("unchecked")
    public static Map<String, Object> decode(final CharBuffer data) throws IOException {
        final Object decodedData = decodeAny(data);

        if (decodedData instanceof Map) {
            return (Map<String, Object>) decodedData;
        } else {
            throw new IOException("Malformed JSON! Expected type: Map, got: " + decodedData.getClass().getSimpleName());
        }
    }

    public static Map<String, Object> decode(final Path path, final Charset charset) throws IOException {
        try (final FileChannel fc = FileChannel.open(path, StandardOpenOption.READ)) {
            final ByteBuffer data = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
            final CharBuffer chardata = charset.decode(data);

            return decode(chardata);
        }
    }
}
