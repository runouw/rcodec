/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.hypercodec;

import com.runouw.rcodec.Fixed;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Map;

/**
 *
 * @author zmichaels
 */
public final class BJSONEncoder {
    private static final Charset DEFAULT_CHARSET = Charset.forName("UTF8");
    
    private static void encodeStringSafe(final ByteBuffer data, final String str) {    
        data.put((byte) 's');
        data.put(DEFAULT_CHARSET.encode(str));
        data.put((byte) '\0');
    }
    
    private static void encodeObjectSafe(final ByteBuffer data, final Map<String, Object> obj) throws IOException {
        data.put((byte) '{');
        
        for (Map.Entry<String, Object> pair : obj.entrySet()) {
            encodeStringSafe(data, pair.getKey());
            encodeAnySafe(data, pair.getValue());
        }
        
        data.put((byte) '}');
    }
    
    private static void encodeArraySafe(final ByteBuffer data, final List<Object> array) throws IOException {
        data.put((byte) '[');
        
        for (Object obj : array) {
            encodeAnySafe(data, obj);
        }
        
        data.put((byte) '\0');
        data.put((byte) ']');
    }
    
    private static void encodeFixedSafe(final ByteBuffer data, final Fixed fx) {
        data.put((byte) 'x');
        data.putLong(fx.value);
        data.putLong(fx.scale);
    }
    
    private static void encodeBooleanSafe(final ByteBuffer data, final boolean value) {
        data.put((byte) 'b');
        data.put(value ? (byte) 1 : (byte) 0);
    }
    
    private static void encodeByteSafe(final ByteBuffer data, final byte value) {
        data.put((byte) '1');
        data.put(value);
    }
    
    private static void encodeShortSafe(final ByteBuffer data, final short value) {
        data.put((byte) '2');
        data.putShort(value);
    }
    
    private static void encodeIntSafe(final ByteBuffer data, final int value) {
        data.put((byte) 'i');
        data.putInt(value);
    }
    
    private static void encodeLongSafe(final ByteBuffer data, final long value) {
        data.put((byte) 'l');
        data.putLong(value);
    }
    
    private static void encodeFloatSafe(final ByteBuffer data, final float value) {
        data.put((byte) 'f');
        data.putFloat(value);
    }
    
    private static void encodeDoubleSafe(final ByteBuffer data, final double value) {
        data.put((byte) 'd');
        data.putDouble(value);
    }
    
    private static void encodeBlobSafe(final ByteBuffer data, final ByteBuffer value) {
        data.put((byte) '!');
        data.put(value);
    }
    
    private static void encodeNullSafe(final ByteBuffer data) {
        data.put((byte) '_');
    }
    
    public static void encodeHeader(final ByteBuffer data) {
        data.put(new byte[] {'B', 'J', 'S', 'O', 'N'});
    }
    
    @SuppressWarnings("unchecked")
    private static void encodeAnySafe(final ByteBuffer data, final Object obj) throws IOException {
        if (obj == null) {
            encodeNullSafe(data);
        } else if (obj instanceof Map) {
            encodeObjectSafe(data, (Map<String, Object>) obj);
        } else if (obj instanceof List) {
            encodeArraySafe(data, (List<Object>) obj);
        } else if (obj instanceof String) {
            encodeStringSafe(data, (String) obj);
        } else if (obj instanceof Fixed) {
            encodeFixedSafe(data, (Fixed) obj);
        } else if (obj instanceof Double) {
            encodeDoubleSafe(data, (Double) obj);
        } else if (obj instanceof Long) {
            encodeLongSafe(data, (Long) obj);
        } else if (obj instanceof Boolean) {
            encodeBooleanSafe(data, (Boolean) obj);
        } else if (obj instanceof Integer) {
            encodeIntSafe(data, (Integer) obj);
        } else if (obj instanceof Short) {
            encodeShortSafe(data, (Short) obj);
        } else if (obj instanceof Byte) {
            encodeByteSafe(data, (Byte) obj);
        } else if (obj instanceof Float) {
            encodeFloatSafe(data, (Float) obj);        
        } else if (obj instanceof ByteBuffer) {
            encodeBlobSafe(data, (ByteBuffer) obj);
        } else {
            throw new IOException("Unsupported object type: " + obj.getClass().getSimpleName());
        }
    }
    
    public static void encode(final ByteBuffer output, final Map<String, Object> map) throws IOException {
        encodeObjectSafe(output, map);
    }
}
